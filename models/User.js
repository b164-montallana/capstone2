const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
    firstName: {
        type: String,
        default: "Anon user"
    },
    lastName: {
        type: String,
        default: "Anon user"
    },
    email: {
        type: String,
        required: [true, "email is required"]
    },
    password: {
        type: String,
        required: [true, "password is required"],
        select: true
    },
    isAdmin: {
        type: Boolean,
        default: false
    }
    
})

module.exports = mongoose.model("User", userSchema)