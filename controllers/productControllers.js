const Product = require("../models/Product");


//register a product
module.exports.addProduct = (reqBody) => {
    let newProduct = new Product ({
        name: reqBody.name,
        description: reqBody.description,
        stock: reqBody.stock,
        price: reqBody.price,
        isActive: reqBody.isActive
    });
    return newProduct.save().then((product, error) => {
        if(error) {
            return false;
        } else {
            return true;
        }
    })
}