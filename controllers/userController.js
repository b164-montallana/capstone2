const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");


//register new user
module.exports.registerUser = (reqBody) => {
    return User.findOne({ email : reqBody.email}).then(result => {
        if(result){
            return 'Email already exist'
        } else {
            let newUser = new User ({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                password: bcrypt.hashSync(reqBody.password, 12),
                isAdmin: reqBody.isAdmin
            })
            return newUser.save().then((user, error) => {
                if(error){
                    return false;
                } else {
                    return 'User registration successful'
                }
            })
        }
    })
}

//User login and return a jwt token
module.exports.userLogin = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result => {
        if(result == null){
            return "Invalid email address";
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if(isPasswordCorrect){
                return {accessToken : auth.createToken(result.toObject())}
            } else {
                return 'Invalid passwod';
            }
        }
    })
}

//

