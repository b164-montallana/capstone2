const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
require('dotenv').config();

//Routes
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended:true}));

//routes
app.use("/api/user", userRoutes);
app.use("/api/product", productRoutes);

mongoose.connect(process.env.DB_CONNECTION, {
    useNewUrlParser: true,
    UseUnifiedTopology: true
})


mongoose.connection.once('open', () => console.log('We are now connected to mongoDB Atlas'));

app.listen(process.env.PORT, () => {
    console.log(`EcommerceAPI is now online on port ${process.env.PORT}`);
})