const express = require("express");
const router = express.Router();
const ProductController = require("../controllers/productControllers");
const auth = require("../auth")


//register a product (admin only)
router.post("/reg", auth.verify, (req, res) => {
    const data = {
        product: req.body,
        isAdmin: auth.decode(req.headers.authorization).isAdmin
    }
    if(data.isAdmin) {
    ProductController.addProduct(req.body).then(result => res.send(result));
    } else {
        res.send({ auth: " You're not an admin "})
    }
})

module.exports = router;