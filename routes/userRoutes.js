const express = require("express");
const router = express.Router();
const auth = require("../auth");

//Usercontroller route
const UserController = require("../controllers/userController");

//register a user
router.post("/register", (req, res) => {
    UserController.registerUser(req.body).then(result  => res.send(result));
})

//user login
router.post("/login", (req, res) => {
    UserController.userLogin(req.body).then(result => res.send(result));
})

//verify a user if admin and edit userinfo
router.post("/edit-user", auth.verify, (req,res) => {
    const userData = {
        userId: auth.decode(req.headers.authorization).id,
        isAdmin: req.body.isAdmin
    }
    UserController.editUser(userData).then(result => res.send(result));
})

module.exports = router;